import React from 'react'
import Ingridient from "./ingredient";

const Ingredients = props => {
	return props.ingredients.map(ingredient => {
		let indArr = [];
		for(let i = 0; i < ingredient.amount; i++){
			indArr.push(<Ingridient
				key={ingredient.name + i}
				name={ingredient.name}
			/>);
		}
		return indArr;
	});
};

export default Ingredients;

