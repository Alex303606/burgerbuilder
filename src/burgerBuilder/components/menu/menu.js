import React from 'react'
import './menu.css'
import MenuItem from "./menuItem";
const Menu = props => {
	return (
		<div className="menu">
			<h2>Current Price: {props.totalPrice} soms</h2>
			{
				props.ingredients.map(element => {
					return <MenuItem
						key={element.name}
						add={props.add}
						name={element.name}
						remove={props.remove}
						amount={element.amount}
					/>;
				})
			}
		</div>
	)
};

export default Menu;