import React from 'react'

const MenuItem = props => {
	let disable = true;
	if(props.amount > 0) disable = false;
	return (
		<div className="row">
			<span>{props.name}</span>
			<button onClick={() => props.remove(props.name)} disabled={disable} className="less">Less</button>
			<button onClick={() => props.add(props.name)} className="more">More</button>
		</div>
	)
};

export default MenuItem;